class Visit {
  constructor(values) {
    this.purpose = values.purpose;
    this.name = values.name;
    this.date = values.date;
    this.comment = values.comment;
    this.id = values.id;
  }

  renderCard() {
    this.card = document.createElement('div');
    this.card.classList.add('card');
    this.card.innerHTML += `
                                <p class="headline">${this.name}</p>
                                <p class="text">${this.doctor}</p>
                            `;
    this.showMoreBtn = document.createElement('div');
    this.showMoreBtn.classList.add('show-more');
    this.showMoreBtn.innerText = 'Показать больше';

    this.card.appendChild(this.showMoreBtn);

    this.closeBtn = document.createElement('div');
    this.closeBtn.classList.add('close-card')
    this.closeBtn.innerText = '+';
    this.closeBtn.addEventListener('click', () => {
      this.card.remove();
      this.delVisit(this);
    });
    this.card.prepend(this.closeBtn);

    this.cardBlock = document.getElementById('card-block');
    this.cardBlock.appendChild(this.card);

    // перетягивание карточки
    this.card.setAttribute('draggable', true);
    let shiftY, shiftX, shiftWidth, shiftHeight;
    this.card.addEventListener("dragstart", e => {

      shiftY = e.clientY - this.card.getBoundingClientRect().top;
      shiftX = e.clientX - this.card.getBoundingClientRect().left;
      shiftWidth = this.card.getBoundingClientRect().width;
      shiftHeight = this.card.getBoundingClientRect().height;
    })
    this.card.addEventListener('dragend', e => {
      let cardBlockInfo = this.cardBlock.getBoundingClientRect();

      if (e.pageX - shiftX < cardBlockInfo.left) {
        this.card.style.left = cardBlockInfo.left + 'px';
      } else if (e.pageX - shiftX > cardBlockInfo.right - shiftWidth) {
        this.card.style.left = cardBlockInfo.right - shiftWidth + 'px';
      } else if (e.pageY - shiftY < cardBlockInfo.top) {
        this.card.style.top = cardBlockInfo.top + 'px';
      } else if (e.pageY - shiftY > cardBlockInfo.bottom - shiftHeight) {
        this.card.style.top = cardBlockInfo.bottom - shiftHeight + 'px';
      } else {
        this.card.style.left = e.pageX - shiftX + 'px';
        this.card.style.top = e.pageY - shiftY + 'px';
      }

    })


    setRandomPostionInsideContainer(this.card, this.cardBlock);
    function setRandomPostionInsideContainer(htmlElement, container) {
      let containerRect = container.getBoundingClientRect();
      let x = getRandomValue(containerRect.left, containerRect.right - 250);
      let y = getRandomValue(containerRect.top, containerRect.bottom - 150);
      htmlElement.style.top = y + "px";
      htmlElement.style.left = x + "px";

      function getRandomValue(min, max) {
        return Math.random() * (max - min) + min;
      }
    }
  }

  sendVisit(visit) {
    let visitJSON = JSON.stringify(visit);
    let request = fetch("http://cards.danit.com.ua/cards", {
      method: "POST",
      headers: {
        "Content-type": "application/json;charset=uft-8",
        Authorization: `Bearer ${token}`
      },
      body: visitJSON
    })
    request
      .then(response => response.json())
      .then(response => {
        visit.id = response.id;
        console.log(visit.id)
      })

  }

  delVisit(visit) {
    console.log(visit.id)
    let request = fetch(`http://cards.danit.com.ua/cards/${visit.id}`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    request
      .then(message => message.json())
      .then(message => console.log(message))
  }

  static renderAllCards(token) {
    let request = fetch("http://cards.danit.com.ua/cards", {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
    request
      .then(message => message.json())
      // .then(message => message.forEach(function(element){
      //     let request = fetch(`http://cards.danit.com.ua/cards/${element.id}`, {
      //         method: "DELETE",
      //         headers: {
      //             Authorization: `Bearer ${token}`
      //         }
      //     })
      // }))
      .then(message => message.forEach(function (element) {
        if (element.type === 'cardio') {
          new Cardiologist(element).renderCard(element);
        } else if (element.type === 'dentist') {
          new Dentist(element).renderCard(element);
        } else if (element.type === 'therapist') {
          new Therapist(element).renderCard(element);
        }
      }))
  }

  static renderModal() {
    let modalWrapper = document.createElement('div')
    modalWrapper.classList.add('modal-wrapper', 'hide');
    document.body.append(modalWrapper);

    // фон для модального окна
    let modalBackground = document.createElement('div');
    modalBackground.classList.add('background');
    modalBackground.addEventListener('click', Visit.closeModal);
    modalWrapper.appendChild(modalBackground);

    //кнопка закрытия модалки
    let clsBtn = document.createElement('div');
    clsBtn.classList.add('close');
    clsBtn.innerHTML = '+';
    clsBtn.addEventListener('click', Visit.closeModal);

    //создаю select с выбором врачей 
    let select = document.createElement('select');
    select.classList.add("doctor-select");
    select.innerHTML += `   <option disabled selected>Выберите врача</option>
                                        <option value="Кардиолог">Кардиолог</option>
                                        <option value="Стоматолог">Стоматолог</option>
                                        <option value="Терапевт">Терапевт</option>
                                    `;

    // размещение элементов в модальном окне
    let modal = document.createElement('div');
    modal.classList.add('modal');
    modal.appendChild(select);
    modal.prepend(clsBtn);
    let inputListContainer = document.createElement('div');
    inputListContainer.classList.add("input-list-container");
    modal.append(inputListContainer);
    modalWrapper.appendChild(modal);

    //рендеринг доп полей в зависимости от значения в select
    select.addEventListener('change', () => {
      let container = document.querySelector(".input-list-container");
      if (select.value === 'Кардиолог') {
        Visit.renderInputList(Cardiologist.inputList, container)
      } else if (select.value === 'Стоматолог') {
        Visit.renderInputList(Dentist.inputList, container)
      } else if (select.value === 'Терапевт') {
        Visit.renderInputList(Therapist.inputList, container)
      }
      container.append(createBtn)
    })

    let createBtn = document.createElement('div');
    createBtn.classList = "btn btn-create-card";
    createBtn.innerText = "Create card";

    //создание экземляров соответствущих классов в зависимости от значения в select
    createBtn.addEventListener('click', () => {
      let newVisit;
      if (select.value === 'Кардиолог') {
        let values = Visit.readInputValues(Cardiologist.inputList);
        newVisit = new Cardiologist(values);
      } else if (select.value === 'Стоматолог') {
        let values = Visit.readInputValues(Dentist.inputList);
        newVisit = new Dentist(values);
      } else if (select.value === 'Терапевт') {
        let values = Visit.readInputValues(Therapist.inputList);
        newVisit = new Therapist(values);
      }
      newVisit.renderCard();
      newVisit.sendToDB();
      Visit.closeModal();
    })
  }

  static readInputValues(inputList) {
    let values = {};
    for (let input of inputList) {
      values[input.propertyName] = document.querySelector(`#${input.propertyName}`).value;
    }
    return values;
  }

  static renderInputList(inputList, container) {
    document.querySelector(".input-list-container").innerHTML = '';

    for (let input of inputList) {
      if (input.type === "input") {
        let inputElement = document.createElement('input');
        inputElement.setAttribute("id", input.propertyName);
        inputElement.placeholder = input.text;
        console.log(container);
        container.append(inputElement);
      } else if (input.type === "textarea") {
        let inputElement = document.createElement('textarea');
        inputElement.setAttribute("id", input.propertyName);
        inputElement.placeholder = input.text;
        inputElement.setAttribute('maxlength', '400');
        container.append(inputElement);
      }
    }
  }

  static closeModal() {
    let modal = document.querySelector(".modal-wrapper");
    modal.classList.add('hide')
    let elements = document.getElementsByTagName('input')
    for (let element of elements) {
      element.value = ''
    }
    let txtarea = document.querySelector('textarea');
    txtarea.value = ''
  }
}

class Cardiologist extends Visit {
  constructor(values) {
    super(values);
    this.bloodPressure = values.bloodPressure;
    this.bodyMassIndex = values.bodyMassIndex;
    this.diseases = values.diseases;
    this.age = values.age;
    this.doctor = "Кардиолог"
  }
  sendToDB() {
    this.type = 'cardio';
    this.sendVisit(this);
  }

  renderCard() {
    super.renderCard();
    this.showMoreBtn.addEventListener('click', () => {
      this.showMoreBtn.remove();
      this.closeBtn.remove();
      this.card.innerHTML += `
                                    <p class="text">${this.purpose}</p>
                                    <p class="text">${this.bloodPressure}</p>
                                    <p class="text">${this.bodyMassIndex}</p>
                                    <p class="text">${this.diseases}</p>
                                    <p class="text">${this.age}</p>
                                    <p class="text">${this.comment}</p>
                                `
      this.card.prepend(this.closeBtn);
    });
  }

  static inputList = [
    {
      type: "input",
      text: "Цель визита",
      propertyName: "purpose"
    },
    {
      type: "input",
      text: "Обычное кровяное давление",
      propertyName: "bloodPressure"
    },
    {
      type: "input",
      text: "Индекс массы тела",
      propertyName: "bodyMassIndex"
    },
    {
      type: "input",
      text: "Перенесенные заболевания сердечно-сосудистой системы",
      propertyName: "diseases"
    },
    {
      type: "input",
      text: "Возраст",
      propertyName: "age"
    },
    {
      type: "input",
      text: "ФИО",
      propertyName: "name"
    },
    {
      type: "textarea",
      text: "Дополнительный комментарий",
      propertyName: "comment"
    }

  ]
}

class Dentist extends Visit {
  constructor(values) {
    super(values);
    this.lastVisitDate = values.lastVisitDate;
    this.doctor = "Стоматолог";
  }

  sendToDB() {
    this.type = 'dentist';
    this.sendVisit(this)
  }

  renderCard() {
    super.renderCard();
    this.showMoreBtn.addEventListener('click', () => {
      this.showMoreBtn.remove();
      this.closeBtn.remove()
      this.card.innerHTML += `
                                        <p class="text">${this.lastVisitDate}</p>
                                        <p class="text">${this.purpose}</p>
                                        <p class="text">${this.comment}</p>
                                    `
      this.card.prepend(this.closeBtn);
    })
  }

  static inputList = [
    {
      type: "input",
      text: "Цель визита",
      propertyName: "purpose"
    },
    {
      type: "input",
      text: "Дата последнего посещения",
      propertyName: "lastVisitDate"
    },
    {
      type: "input",
      text: "ФИО",
      propertyName: "name"
    },
    {
      type: "textarea",
      text: "Дополнительный комментарий",
      propertyName: "comment"
    }
  ]
}

class Therapist extends Visit {
  constructor(values) {
    super(values);
    this.age = values.age;
    this.doctor = "Терапевт";
  }

  sendToDB() {
    this.type = 'therapist';
    this.sendVisit(this)
  }

  renderCard() {
    super.renderCard();
    this.showMoreBtn.addEventListener('click', () => {
      this.showMoreBtn.remove();
      this.closeBtn.remove()
      this.card.innerHTML += `
                                        <p class="text">${this.age}</p>
                                        <p class="text">${this.purpose}</p>
                                        <p class="text">${this.comment}</p>
                                    `
      this.card.prepend(this.closeBtn);
    })
  }

  static inputList = [
    {
      type: "input",
      text: "Цель визита",
      propertyName: "purpose"
    },
    {
      type: "input",
      text: "Возраст",
      propertyName: "age"
    },
    {
      type: "input",
      text: "ФИО",
      propertyName: "name"
    },
    {
      type: "textarea",
      text: "Дополнительный комментарий",
      propertyName: "comment"
    }
  ]
}


//creating header, wrappers, modal (yet hidden)
let wrapper = document.createElement('div');
wrapper.classList.add("wrapper");
let websiteWrapper = document.createElement('div');
websiteWrapper.classList.add('website-wrapper');
wrapper.appendChild(websiteWrapper);

let header = document.createElement('header');
let logo = document.createElement('div');
logo.classList.add('logo');
logo.innerHTML = '<span class="logo-blue">med</span><span>cards</span>'
let menu = document.createElement('div');
menu.classList.add('menu')
let createCardBtn = document.createElement('div')
createCardBtn.classList = "btn btn-create-card";
createCardBtn.innerText = "Create card";
menu.classList.add('menu');
menu.appendChild(createCardBtn);
header.appendChild(logo);
header.appendChild(menu);
websiteWrapper.appendChild(header);

let cardBlock = document.createElement('div');
cardBlock.classList.add('card-block');
cardBlock.id = 'card-block';
websiteWrapper.appendChild(cardBlock);
document.body.appendChild(wrapper);

Visit.renderModal();

//showing  modal
createCardBtn.addEventListener('click', () => {
  document.querySelector(".modal-wrapper").classList.remove('hide')
  document.querySelector(".input-list-container").innerHTML = '';
  document.querySelector(".doctor-select").options[0].selected = true;
});



// WORKING WITH DATA BASE:

let account = {
  email: "ovcherenko.b@gmail.com",
  password: "qwerty"
}

//Checking token and recreating cards from DB
if (sessionStorage.getItem('token')) {
  let token = sessionStorage.getItem('token');
  Visit.renderAllCards(token);
} else {
  login(account)
    .then((token) => {
      Visit.renderAllCards(token);
      sessionStorage.setItem('token', token)
    });
}

function login(account) {
  return fetch("http://cards.danit.com.ua/login", {
    method: "POST",
    headers: {
      "Content-Type": "application/json;charset=utf-8"
    },
    body: JSON.stringify(account)
  }).then(message => message.json())
    .then(message => {
      let token = message.token;
      return token;
    })
}

// same function but using axios

// function login(account) {
//   return axios.post("http://cards.danit.com.ua/login", JSON.stringify(account)
//     ).then(message => {
//       let token = message.data.token;
//       return token;
//     })
// }

// async function login(account) {
//   let response = await axios.post("http://cards.danit.com.ua/login", JSON.stringify(account));
//   let token = response.data.token;
//   return token;
// }