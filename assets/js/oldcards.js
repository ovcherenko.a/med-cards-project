class Visit {
    constructor() {

    }

    render() {
        //создаем шапку
        this.wrapper = document.createElement('div');       
        this.wrapper.classList.add("wrapper");
        this.websiteWrapper = document.createElement('div');
        this.websiteWrapper.classList.add('website-wrapper');
        this.wrapper.appendChild(this.websiteWrapper);

        this.header = document.createElement('header');
        this.logo = document.createElement('div');
        this.logo.classList.add('logo');
        this.logo.innerHTML = '<span class="logo-blue">med</span><span>cards</span>'
        this.menu = document.createElement('div');
        this.menu.classList.add('menu')
        this.createCardBtn = document.createElement('div')
        this.createCardBtn.classList = "btn btn-create-card";
        this.createCardBtn.innerText = "Create card";
        this.menu.classList.add('menu');

        this.menu.appendChild(this.createCardBtn);
        this.header.appendChild(this.logo);
        this.header.appendChild(this.menu);
        this.websiteWrapper.appendChild(this.header);
         
        this.renderModal()
        
        this.createCardBtn.addEventListener('click', () => {this.modalWrapper.classList.remove('hide')});

        this.cardBlock = document.createElement('div');
        this.cardBlock.classList.add('card-block');
        this.cardBlock.id = 'card-block';
        this.websiteWrapper.appendChild(this.cardBlock);
        document.body.appendChild(this.wrapper);
    }

    // removeListeners() {
    //     this.modalBtn.removeEventListener('click', this.createEventCardCardiologist);
    //     this.modalBtn.removeEventListener('click', this.createEventCardDentist);
    //     this.modalBtn.removeEventListener('click', this.createEventCardTherapist);
    // }

    renderModal() {
        
        // создание модального окна
        this.modalWrapper = document.createElement('div')
        this.modalWrapper.classList.add('modal-wrapper', 'hide');

        // фон для модального окна
        this.modalBackground = document.createElement('div');
        this.modalBackground.classList.add('background');
        this.modalWrapper.appendChild(this.modalBackground);
        

        // this.closeBtn.onclick = () => {this.modalWrapper.classList.add('hide')};


        //создаю select с выбором врачей 
        this.select = document.createElement('select');
        this.select.innerHTML +=`   <option value="Кардиолог">Кардиолог</option>
                                    <option value="Стоматолог">Стоматолог</option>
                                    <option value="Терапевт">Терапевт</option>
                                `

        // размещение элементов в модальном окне
        this.modal = document.createElement('div');
        this.modal.classList.add('modal');
        this.modalWrapper.appendChild(this.modal)

        this.modal.innerHTML += `<p class="text">Выберите врача</p>`
        this.modal.appendChild(this.select);

        // размещение списка инпутов
        this.inputList = document.createElement('div');
        // this.renderCardiologistInput();

        this.modal.appendChild(this.inputList);

        //размещение кнопки
        // this.modalBtn = document.createElement('div');
        // this.modalBtn.classList.add('btn');
        // this.modalBtn.innerText = "Создать карточку";
        // this.modalBtn.addEventListener('click', this.createEventCardCardiologist);
        // this.modal.appendChild(this.modalBtn);





        //события кнопок
        this.select.addEventListener('change', () => {
            if(this.select.value === 'Кардиолог') {
                this.inputList.innerHTML = '';

                this.inputPurpose = document.createElement('input');
                this.inputPurpose.placeholder = "Цель визита";
                this.inputList.appendChild(this.inputPurpose);
        
                this.inputBloodPressure = document.createElement('input');
                this.inputBloodPressure.placeholder = "Обычное кровяное давление";
                this.inputList.appendChild(this.inputBloodPressure);
        
                this.inputBodyMassIndex = document.createElement('input');
                this.inputBodyMassIndex.placeholder = "Индекс массы тела";
                this.inputList.appendChild(this.inputBodyMassIndex);
        
                this.inputDiseases = document.createElement('input');
                this.inputDiseases.placeholder = "Перенесенные заболевания сердечно-сосудистой системы";
                this.inputList.appendChild(this.inputDiseases);
        
                this.inputAge = document.createElement('input');
                this.inputAge.placeholder = "Возраст";
                this.inputList.appendChild(this.inputAge);
        
                this.inputName = document.createElement('input');
                this.inputName.placeholder = "ФИО";
                this.inputList.appendChild(this.inputName);
                // this.renderCardiologistInput.bind(this);


                this.modalBtn = document.createElement('div');
                this.modalBtn.classList.add('btn');
                this.modalBtn.innerText = "Создать карточку";
                this.modalBtn.addEventListener('click', this.createEventCardCardiologist);
                this.inputList.appendChild(this.modalBtn)
            }
        }
        )
            //     this.modalBtn.addEventListener('click', this.createEventCardCardiologist.bind(this));
            // } else if (this.select.value === 'Стоматолог'){
            //     this.renderDentistInput();
            //     this.modalBtn.addEventListener('click', this.createEventCardDentist.bind(this));
            // } else if (this.select.value === 'Терапевт') {
            //     this.renderTherapistInput();
            //     this.modalBtn.addEventListener('click', this.createEventCardTherapist.bind(this));
            // }
        // })

      
        this.modalBackground.addEventListener('click', () => {
            this.modalWrapper.classList.add('hide')
            let elements = this.inputList.getElementsByTagName('input')
            for(let i = 0; i < elements.length; i++) {
                elements[i].value = ''
            }
        })

        // кнока закрыть модальное окно
        this.clsBtn = document.createElement('div');
        this.clsBtn.classList.add('close');
        this.clsBtn.innerHTML = '+';
        this.clsBtn.addEventListener('click', () => {
            this.modalWrapper.classList.add('hide')
            let elements = this.inputList.getElementsByTagName('input')
            for(let i = 0; i < elements.length; i++) {
                elements[i].value = ''
            }
        })
        this.modal.prepend(this.clsBtn);
        
        
        document.body.appendChild(this.modalWrapper);
    }

    // формирование input для кардиолога
    // renderCardiologistInput() {
    //     this.inputList.innerHTML = '';

    //     this.inputPurpose = document.createElement('input');
    //     this.inputPurpose.placeholder = "Цель визита";
    //     this.inputList.appendChild(this.inputPurpose);

    //     this.inputBloodPressure = document.createElement('input');
    //     this.inputBloodPressure.placeholder = "Обычное кровяное давление";
    //     this.inputList.appendChild(this.inputBloodPressure);

    //     this.inputBodyMassIndex = document.createElement('input');
    //     this.inputBodyMassIndex.placeholder = "Индекс массы тела";
    //     this.inputList.appendChild(this.inputBodyMassIndex);

    //     this.inputDiseases = document.createElement('input');
    //     this.inputDiseases.placeholder = "Перенесенные заболевания сердечно-сосудистой системы";
    //     this.inputList.appendChild(this.inputDiseases);

    //     this.inputAge = document.createElement('input');
    //     this.inputAge.placeholder = "Возраст";
    //     this.inputList.appendChild(this.inputAge);

    //     this.inputName = document.createElement('input');
    //     this.inputName.placeholder = "ФИО";
    //     this.inputList.appendChild(this.inputName);
        
        // this.modal.appendChild('ololo')
        // console.log(this.modal)

        // this.modal.appendChild(this.modalBtn)
    // }

    // формирование input для дантиста
    renderDentistInput() {
        this.inputList.innerHTML = '';

        this.inputPurpose = document.createElement('input');
        this.inputPurpose.placeholder = "Цель визита";
        this.inputList.appendChild(this.inputPurpose);

        this.lastVisitDate = document.createElement('input');
        this.lastVisitDate.placeholder = "Дата последнего посещения";
        // this.inputDate.type="date";
        this.inputList.appendChild(this.lastVisitDate);

        this.inputName = document.createElement('input');
        this.inputName.placeholder = "ФИО";
        this.inputList.appendChild(this.inputName);
    }

    // формирование input для педатора
    renderTherapistInput() {
        this.inputList.innerHTML = '';

        this.inputPurpose = document.createElement('input');
        this.inputPurpose.placeholder = "Цель визита";
        this.inputList.appendChild(this.inputPurpose);


        this.inputAge = document.createElement('input');
        this.inputAge.placeholder = "Возраст";
        // this.inputAge.type="date";
        this.inputList.appendChild(this.inputAge);

        this.inputName = document.createElement('input');
        this.inputName.placeholder = "ФИО";
        this.inputList.appendChild(this.inputName);
    }

    createEventCardCardiologist(elem) {

        console.log(this.inputPurpose)

        let cardiologist = new Cardiologist(this.inputName.value, this.inputDiseases.value, this.inputBloodPressure.value, this.inputBodyMassIndex.value, this.inputDiseases.value, this.inputAge.value);
        for(let i=0; i < elem.path[1].childNodes[3].childNodes.length; i++){
            elem.path[1].childNodes[3].childNodes[i].value = '';
        }
        cardiologist.render();
        elem.path[2].classList.add('hide')
        console.log('cardiologist')

   }

    createEventCardDentist(elem) {

        console.log(this.inputName)

        
        let dentist = new Dentist(this.inputName.value, this.inputPurpose.value)
        for(let i=0; i < elem.path[1].childNodes[3].childNodes.length; i++){
            elem.path[1].childNodes[3].childNodes[i].value = '';
        }
        dentist.render();
        elem.path[2].classList.add('hide')
        console.log('Dantist')
    }

    createEventCardTherapist(elem) {
        let therapist = new Therapist(this.inputName.value, this.inputPurpose.value, this.inputAge.value)
        for(let i=0; i < elem.path[1].childNodes[3].childNodes.length; i++){
            elem.path[1].childNodes[3].childNodes[i].value = '';
        }
        therapist.render();
        elem.path[2].classList.add('hide')
        console.log('Therapist')
    }

}

class Cardiologist extends Visit{
    constructor(patientName, purpose, bloodPressure, bodyMassIndex, diseases, age, visitName) {
        super(visitName, patientName)
        this.name = name;
        this.patientName = patientName;
        this.purpose = purpose;
        this.bloodPressure = bloodPressure;
        this.bodyMassIndex = bodyMassIndex;
        this.diseases = diseases;
        this.age = age;
    }

    render() {
        this.card = document.createElement('div');
        this.card.classList.add('card');



        this.showMoreBtn = document.createElement('div');
        this.showMoreBtn.classList.add('show-more');
        this.showMoreBtn.innerText = 'Показать больше';
        this.showMoreBtn.addEventListener('click', () => {
            this.showMoreBtn.remove();
            this.closeBtn.remove();
            this.card.innerHTML+=`
                                    <p class="text">${this.purpose}</p>
                                    <p class="text">${this.bloodPressure}</p>
                                    <p class="text">${this.bodyMassIndex}</p>
                                    <p class="text">${this.diseases}</p>
                                    <p class="text">${this.age}</p>
                                `
            this.card.prepend(this.closeBtn);
        })

        this.card.innerHTML+=`
                                <p class="headline">${this.patientName}</p>
                                <p class="text">Кардиолог</p>
                            `
        this.card.appendChild(this.showMoreBtn);
        this.cardBlock = document.getElementById('card-block');
        this.cardBlock.appendChild(this.card);

        // перетягивание карточки
        this.card.setAttribute('draggable', true);

        this.card.addEventListener('dragend', function(e) {
            this.style.left = e.pageX - this.offsetWidth / 2 + 'px';
            this.style.top = e.pageY - this.offsetHeight / 2 + 'px';
        })
        this.closeBtn = document.createElement('div');
        this.closeBtn.classList.add('close-card')
        this.closeBtn.innerText = '+';
        this.closeBtn.addEventListener('click', () => this.card.remove())
        this.card.prepend(this.closeBtn);
    }

}


class Dentist extends Visit {
    constructor(name, purpose) {
        super()
        this.purpose = purpose;
        this.date = new Date();
        this.name = name;
    }
    render(){
        this.card = document.createElement('div');
        this.card.classList.add('card');        
        this.card.innerHTML=`
                                <p class="headline">${this.name}</p>
                                <p class="text">Стоматолог</p>

                            `
        this.cardBlock = document.getElementById('card-block');
        this.cardBlock.appendChild(this.card);

        this.showMoreBtn = document.createElement('div');
        this.showMoreBtn.classList.add('show-more');
        this.showMoreBtn.innerText = 'Показать больше';
        this.showMoreBtn.addEventListener('click', () => {
            this.showMoreBtn.remove();
            this.closeBtn.remove()
            this.card.innerHTML+=   `
                                        <p class="text">${this.date.getFullYear()}</p>
                                        <p class="text">${this.purpose}</p>
                                    `
            this.card.prepend(this.closeBtn);
        })

        this.card.appendChild(this.showMoreBtn);

        this.card.setAttribute('draggable', true);
    
        this.card.addEventListener('dragend', function(e) {
            this.style.left = e.pageX - this.offsetWidth / 2 + 'px';
            this.style.top = e.pageY - this.offsetHeight / 2 + 'px';
        })

        this.closeBtn = document.createElement('div');
        this.closeBtn.classList.add('close-card')
        this.closeBtn.innerText = '+';
        this.closeBtn.addEventListener('click', () => this.card.classList.add('hide'))
        this.card.prepend(this.closeBtn);
        }
    }


class Therapist extends Visit {
    constructor(name, purpose, age) {
        super()
        this.purpose = purpose;
        this.age = age;
        this.name = name;
    }
    render(){
        this.card = document.createElement('div');
        this.card.classList.add('card');

        this.card.innerHTML=`
                                <p class="headline">${this.name}</p>
                                <p class="text">Терапевт</p>
                            `
        this.cardBlock = document.getElementById('card-block');
        this.cardBlock.appendChild(this.card);
    
        this.showMoreBtn = document.createElement('div');
        this.showMoreBtn.classList.add('show-more');
        this.showMoreBtn.innerText = 'Показать больше';
        this.showMoreBtn.addEventListener('click', () => {
            this.showMoreBtn.remove();
            this.closeBtn.remove()
            this.card.innerHTML+=   `
                                        <p class="text">${this.age}</p>
                                        <p class="text">${this.purpose}</p>
                                    `
            this.card.prepend(this.closeBtn);
        })

        this.card.appendChild(this.showMoreBtn);
        this.cardBlock = document.getElementById('card-block');
        this.cardBlock.appendChild(this.card);


        this.card.setAttribute('draggable', true);

        this.card.addEventListener('dragend', function(e) {
            this.style.left = e.pageX - this.offsetWidth / 2 + 'px';
            this.style.top = e.pageY - this.offsetHeight / 2 + 'px';
        })
        this.closeBtn = document.createElement('div');
        this.closeBtn.classList.add('close-card')
        this.closeBtn.innerText = '+';
        this.closeBtn.addEventListener('click', () => this.card.classList.add('hide'))
        this.card.prepend(this.closeBtn);
        }

}

let visit = new Visit;
visit.render();
